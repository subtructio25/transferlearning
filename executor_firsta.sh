#!/bin/bash
LIST_SCRIPTS=(
    'amazon;caltech;0.001'
    'amazon;dslr;0.001'
    'amazon;webcam;0.001'
    'caltech;amazon;0.001'
    'caltech;dslr;0.001'
    'caltech;webcam;0.001'
    'webcam;amazon;0.001'
    'webcam;caltech;0.001'
    'webcam;dslr;0.001'
    'dslr;amazon;0.001'
    'dslr;webcam;0.001'
    'dslr;caltech;0.001'
)

for item in "${LIST_SCRIPTS[@]}"; do
  # process "$i"
  IFS=';' read -ra ABITEM <<< "$item"
  python main_task_14.py ${ABITEM[0]} ${ABITEM[1]} ${ABITEM[2]}
done
