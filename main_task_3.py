#!/usr/bin/env python
# coding: utf-8

# In[246]:



#import all libraries needed
import tensorflow as tf
import glob 
import numpy as np
import pandas as pd
from mlxtend.data import loadlocal_mnist
import math
from sklearn.metrics import confusion_matrix, accuracy_score, precision_score, recall_score, f1_score
from sklearn import svm
from sklearn import model_selection
import sys

import sklearn.metrics
import sklearn.neighbors
from sklearn import metrics
from sklearn import svm
from sklearn.model_selection import train_test_split

PREFIX_SOURCE = sys.argv[1]
PREFIX_TARGET = sys.argv[2]
DATA_EXCEL = []


# Matrix Measurements at Caculate Means step
SOURCE_CHOSEX = np.asarray(pd.read_excel(r'%s_surf_10_feas.xlsx'%PREFIX_SOURCE))
SOURCE_Y = np.asarray(pd.read_excel(r'%s_surf_10_label.xls'%PREFIX_SOURCE))
TARGET_CHOSEX = np.asarray(pd.read_excel(r'%s_surf_10_feas.xlsx'%PREFIX_TARGET))
TARGET_Y = np.asarray(pd.read_excel(r'%s_surf_10_label.xls'%PREFIX_TARGET))

TARGET_PROCESS = []
SOURCE_PROCESS = []
# dibutuhkan  pada block line 19 
TARGET_PROCESS.append(TARGET_CHOSEX)
SOURCE_PROCESS.append(SOURCE_CHOSEX)


# In[248]:


TARGET_CHOSEY = []
SOURCE_CHOSEY = []
for i in range(len(TARGET_Y)):
  TARGET_CHOSEY.append(TARGET_Y[i])
for i in range(len(SOURCE_Y)):
  SOURCE_CHOSEY.append(SOURCE_Y[i])
print(len(TARGET_CHOSEY))
print(len(SOURCE_CHOSEY))


# In[249]:


minElement = np.amin(TARGET_CHOSEX)
maxElement = np.amax(TARGET_CHOSEX)

TARGET_CHOSEX = (TARGET_CHOSEX-minElement)/(maxElement - minElement)


# In[250]:


minElement = np.amin(SOURCE_CHOSEX)
maxElement = np.amax(SOURCE_CHOSEX)

SOURCE_CHOSEX = (SOURCE_CHOSEX-minElement)/(maxElement - minElement)


# In[251]:


# Matrix Measurements at Caculate Means step
# SOURCE_DATA = np.asarray(pd.read_excel(r'./amazon_result.xlsx'))
# TARGET_DATA = np.asarray(pd.read_excel(r'./lsr_result.xlsx'))

SOURCE_MEAN = []
TARGET_MEAN = []

for i in range(0, len(SOURCE_CHOSEX[0])): 
    sum_a = 0
    for j in range(0, len(SOURCE_CHOSEX)):
        sum_a = sum_a + SOURCE_CHOSEX[j][i]
    res_a = sum_a/len(SOURCE_CHOSEX[j])
    SOURCE_MEAN.append(res_a)

for i in range(0, len(TARGET_CHOSEX[0])): 
    sum_a = 0
    for j in range(0, len(TARGET_CHOSEX)):
        try:
            sum_a = sum_a + TARGET_CHOSEX[j][i]
        except:
            print("An exception occurred")
    res_a = sum_a/len(TARGET_CHOSEX[j])
    TARGET_MEAN.append(res_a)
#calculate for SVHN then 


# In[252]:


#Matrix Measurements Method Call
#maximum mean discrepancy
def mmd(a, b, len_a, len_b):
    return abs(( ((1/len_a)*a) - ((1/len_b)*b) ))


# In[253]:


def kernel(params, sv, X):
    return [np.dot(vi, np.transpose(X)) for vi in sv]


def decision_function(params, sv, nv, a, b, X):
    k = kernel(params, sv, X)

    start = [sum(nv[:i]) for i in range(len(nv))]
    end = [start[i] + nv[i] for i in range(len(nv))]

    c = [ sum(a[ i ][p] * k[p] for p in range(start[j], end[j])) +
          sum(a[j-1][p] * k[p] for p in range(start[i], end[i]))
                for i in range(len(nv)) for j in range(i+1,len(nv))]

    return [sum(x) for x in zip(c, b)]

def predict(params, sv, nv, a, b, cs, X):
    decision = decision_function(params, sv, nv, a, b, X)
    votes = [i if decision[p] > 0 else j for p,(i,j) in enumerate(
            (i,j) 
            for i in range(len(cs))
            for j in range(i+1,len(cs))
    )]
# 
    return votes   


# In[254]:


#Matrix Measurements at Caculate by Call Method step
MATRIX_MEASURE = []
for i in range(len(SOURCE_MEAN)):
    MATRIX_MEASURE.append((mmd(SOURCE_MEAN[i], TARGET_MEAN[i], len(SOURCE_CHOSEX), len(TARGET_CHOSEX)))**2)

MM_MEAN = np.asarray(MATRIX_MEASURE).mean()


# In[ ]:


#Data set source akan di cek dengan clasiifer svm dimana setiap bobot nya diambil dari MM_MATRIX sebelum diterapkan policy
clf = svm.LinearSVC(C=1.0, dual=True, fit_intercept=True,
          intercept_scaling=1, loss='squared_hinge', max_iter=1000,
          multi_class='ovr', penalty='l2', random_state=None, tol=0.0001,
          verbose=0)
# Fit model using features, X, and labels, Y.

METRIC_X = np.dot(SOURCE_CHOSEX, MATRIX_MEASURE)

clf.fit(SOURCE_CHOSEX, SOURCE_CHOSEY,METRIC_X)

METRIC_TARGET = np.multiply(TARGET_CHOSEX, MATRIX_MEASURE)

PRED_RESULT = clf.predict(METRIC_TARGET)

# new data set with pseudo label from previous classification
print("Pseudolabel")
NEW_TARGET_TRAIN_X, NEW_TARGET_TEST_X, NEW_TARGET_TRAIN_Y, NEW_TARGET_TEST_Y = train_test_split(TARGET_CHOSEX, PRED_RESULT)

print(NEW_TARGET_TRAIN_X.shape)
print(NEW_TARGET_TRAIN_Y.shape)
print("== Here")

clf.fit(NEW_TARGET_TRAIN_X, NEW_TARGET_TRAIN_Y)

NEW_PRED_RESULT = clf.predict(NEW_TARGET_TEST_X)

matrix = confusion_matrix(NEW_TARGET_TEST_Y, NEW_PRED_RESULT)
print(matrix)
metric_akur = accuracy_score(NEW_TARGET_TEST_Y, NEW_PRED_RESULT)
print(metric_akur)
metric_preci = precision_score(NEW_TARGET_TEST_Y, NEW_PRED_RESULT, average='weighted')
print(metric_preci)
metric_recal = recall_score(NEW_TARGET_TEST_Y, NEW_PRED_RESULT,  average='weighted')
print(metric_recal)
metric_balap = f1_score(NEW_TARGET_TEST_Y, NEW_PRED_RESULT, average='weighted')
print(metric_balap)

#no policy
DATA_EXCEL.append([PREFIX_SOURCE+" "+PREFIX_TARGET+" LS-1"])
DATA_EXCEL.append(["Accuracy"]);
DATA_EXCEL.append([metric_akur]);
DATA_EXCEL.append(["Precision"]);
DATA_EXCEL.append([metric_preci]);
DATA_EXCEL.append(["Recal"]);
DATA_EXCEL.append([metric_recal]);
DATA_EXCEL.append(["F1"]);
DATA_EXCEL.append([metric_balap]);

# In[256]:


#Choose features by policy
CHOOSE_MATRIX = []
CHOOSE_FEATURE = []

for i in range(len(MATRIX_MEASURE)):
    if(MATRIX_MEASURE[i] <= MM_MEAN and MATRIX_MEASURE[i] != 0):
        CHOOSE_FEATURE.append(i)
        CHOOSE_MATRIX.append(MATRIX_MEASURE[i])


# In[257]:


#Calculate Weights
WEIGHT_MATRIX = []
MM_TOTAL = np.sum(np.array(MATRIX_MEASURE))

REAL_WEIGHTMATRIX = []
for j in range(len(MATRIX_MEASURE)):
    if(np.isin(j, CHOOSE_FEATURE)):
        res = ((1/len(CHOOSE_MATRIX))*MM_TOTAL)/MATRIX_MEASURE[j]
        if(np.isfinite(res)):
            WEIGHT_MATRIX.append(res)
            REAL_WEIGHTMATRIX.append(res)
        else:
            WEIGHT_MATRIX.append(sys.maxint)
    else:
        WEIGHT_MATRIX.append(0)


# In[258]:


##SVM Classifier Kernel RBF
clf = svm.LinearSVC(C=1.0, dual=True, fit_intercept=True,
          intercept_scaling=1, loss='squared_hinge', max_iter=1000,
          multi_class='ovr', penalty='l2', random_state=None, tol=0.0001,
          verbose=0)
# Fit model using features, X, and labels, Y.

METRIC_X= np.dot(SOURCE_CHOSEX , WEIGHT_MATRIX)


clf.fit(SOURCE_CHOSEX, SOURCE_CHOSEY,METRIC_X)

METRIC_TARGET = np.multiply(TARGET_CHOSEX, WEIGHT_MATRIX)

PRED_RESULT = clf.predict(METRIC_TARGET)

NEW_TARGET_TRAIN_X, NEW_TARGET_TEST_X, NEW_TARGET_TRAIN_Y, NEW_TARGET_TEST_Y = train_test_split(TARGET_CHOSEX, PRED_RESULT)

clf.fit(NEW_TARGET_TRAIN_X, NEW_TARGET_TRAIN_Y)

NEW_PRED_RESULT = clf.predict(NEW_TARGET_TEST_X)

matrix = confusion_matrix(NEW_TARGET_TEST_Y, NEW_PRED_RESULT)
print(matrix)
metric_akur = accuracy_score(NEW_TARGET_TEST_Y, NEW_PRED_RESULT)
print(metric_akur)
metric_preci = precision_score(NEW_TARGET_TEST_Y, NEW_PRED_RESULT, average='weighted')
print(metric_preci)
metric_recal = recall_score(NEW_TARGET_TEST_Y, NEW_PRED_RESULT,  average='weighted')
print(metric_recal)
metric_balap = f1_score(NEW_TARGET_TEST_Y, NEW_PRED_RESULT, average='weighted')
print(metric_balap)

#no policy
DATA_EXCEL.append([PREFIX_SOURCE+" "+PREFIX_TARGET+" LS-21"])
DATA_EXCEL.append(["Accuracy"]);
DATA_EXCEL.append([metric_akur]);
DATA_EXCEL.append(["Precision"]);
DATA_EXCEL.append([metric_preci]);
DATA_EXCEL.append(["Recal"]);
DATA_EXCEL.append([metric_recal]);
DATA_EXCEL.append(["F1"]);
DATA_EXCEL.append([metric_balap]);


# In[259]:


#Choose features by policy
CHOOSE_MATRIX = []
CHOOSE_FEATURE = []

for i in range(len(MATRIX_MEASURE)):
    if(MATRIX_MEASURE[i] <= MM_MEAN and MATRIX_MEASURE[i] != 0):
        CHOOSE_FEATURE.append(i)
        CHOOSE_MATRIX.append(MATRIX_MEASURE[i])


# In[260]:


#Calculate Weights
WEIGHT_MATRIX = []
MM_TOTAL = np.sum(np.array(MATRIX_MEASURE))

REAL_WEIGHTMATRIX = []
for j in range(len(MATRIX_MEASURE)):
    if(np.isin(j, CHOOSE_FEATURE)):
        res = 1
        WEIGHT_MATRIX.append(res)
        REAL_WEIGHTMATRIX.append(res)
    else:
        WEIGHT_MATRIX.append(0)


# In[261]:



clf = svm.LinearSVC(C=1.0, dual=True, fit_intercept=True,
          intercept_scaling=1, loss='squared_hinge', max_iter=1000,
          multi_class='ovr', penalty='l2', random_state=None, tol=0.0001,
          verbose=0)
# Fit model using features, X, and labels, Y.

METRIC_X= np.dot(SOURCE_CHOSEX , WEIGHT_MATRIX)

clf.fit(SOURCE_CHOSEX, SOURCE_CHOSEY,METRIC_X)

METRIC_TARGET = np.multiply(TARGET_CHOSEX, WEIGHT_MATRIX)

PRED_RESULT = clf.predict(METRIC_TARGET)

NEW_TARGET_TRAIN_X, NEW_TARGET_TEST_X, NEW_TARGET_TRAIN_Y, NEW_TARGET_TEST_Y = train_test_split(TARGET_CHOSEX, PRED_RESULT)

clf.fit(NEW_TARGET_TRAIN_X, NEW_TARGET_TRAIN_Y)

NEW_PRED_RESULT = clf.predict(NEW_TARGET_TEST_X)

matrix = confusion_matrix(NEW_TARGET_TEST_Y, NEW_PRED_RESULT)
print(matrix)
metric_akur = accuracy_score(NEW_TARGET_TEST_Y, NEW_PRED_RESULT)
print(metric_akur)
metric_preci = precision_score(NEW_TARGET_TEST_Y, NEW_PRED_RESULT, average='weighted')
print(metric_preci)
metric_recal = recall_score(NEW_TARGET_TEST_Y, NEW_PRED_RESULT,  average='weighted')
print(metric_recal)
metric_balap = f1_score(NEW_TARGET_TEST_Y, NEW_PRED_RESULT, average='weighted')
print(metric_balap)

#no policy
DATA_EXCEL.append([PREFIX_SOURCE+" "+PREFIX_TARGET+" LS-22"])
DATA_EXCEL.append(["Accuracy"]);
DATA_EXCEL.append([metric_akur]);
DATA_EXCEL.append(["Precision"]);
DATA_EXCEL.append([metric_preci]);
DATA_EXCEL.append(["Recal"]);
DATA_EXCEL.append([metric_recal]);
DATA_EXCEL.append(["F1"]);
DATA_EXCEL.append([metric_balap]);


# In[262]:


#Get mean of every instances on MNIST
SOURCE_DATA_EACH_MEAN = []

for i in range(0, len(SOURCE_CHOSEX)):
    SOURCE_DATA_EACH_MEAN.append(np.mean(np.array(SOURCE_CHOSEX[i])))
    
#Get mean of every instances on SVHN
TARGET_DATA_EACH_MEAN = []

for i in range(0, len(TARGET_CHOSEX)):
    TARGET_DATA_EACH_MEAN.append(np.mean(np.array(TARGET_CHOSEX[i])))


# In[263]:


# SOURCE Cluster
SOURCE_CENTRE = {}
SOURCE_CLUSTSIZE = []
for i in range (0, 10):
    SOURCE_CENTRE["%s"%i] = 0
    
# SOURCE_X, SOURCE_Y
for i in range (0, 10):
    SOURCE_CLUSTSIZE.append(0)
    for j in range(0, len(SOURCE_CHOSEX)):
        if(SOURCE_CHOSEY[j] == i):
            SOURCE_CLUSTSIZE[i] = SOURCE_CLUSTSIZE[i]+1
            SOURCE_CENTRE["%s"%i]= SOURCE_CENTRE["%s"%i] + np.mean(SOURCE_CHOSEX[j])

for i in range(0, len(SOURCE_CENTRE)):
    # ditambahan dikarenakan ada hasil clustersize yang 0
    if SOURCE_CLUSTSIZE[i] == 0 :
      SOURCE_CLUSTSIZE[i] = 1
    # -------------------------------------------------
    SOURCE_CENTRE["%s"%i] = (1/SOURCE_CLUSTSIZE[i]) * SOURCE_CENTRE["%s"%i]
            
SOURCE_CENTRE


# ### **Penggunaan rumus 4**
# 
# **dilakukan pencarian distance dari data source dan target ke center masing masing pencarian ini digunakan untuk mengetahui nilai terkecil dari source data mean awal ke center terdekat berapa jaraknya.**

# In[264]:


#Calculate distances from SOURCE data to cluster
SOURCE_DIST = []

for i in range(0, len( SOURCE_DATA_EACH_MEAN)):
    SMALLEST = 10000000
    SOURCE_DIST.append(SMALLEST)
    for j in range(0, len(SOURCE_CENTRE)):
        SOURCE_ADIST = np.absolute(SOURCE_DATA_EACH_MEAN[i] - SOURCE_CENTRE["%s"%j])
        if(SOURCE_ADIST < SMALLEST):  
            SOURCE_DIST[i] = SOURCE_ADIST
            SMALLEST = SOURCE_ADIST
SOURCE_DIST


# **setelah nilai terpendek sudah didapt tinggal dikali dengan nilai bobot fitur yang telah terpilih sehingga mengahasilkan nilai seperti dibawah. (masih dalam bentuk terpisah source dan taget belum melebur).**

# In[265]:


#Calculate distances from TARGET data to cluster (source cluster)
TARGET_DIST = []

for i in range(0, len( TARGET_DATA_EACH_MEAN)):
    SMALLEST = 10000000
    TARGET_DIST.append(SMALLEST)
    for j in range(0, len(SOURCE_CENTRE)):
        TARGET_ADIST = np.absolute(TARGET_DATA_EACH_MEAN[i] - SOURCE_CENTRE["%s"%j])
        if(TARGET_ADIST < SMALLEST):  
            TARGET_DIST[i] = TARGET_ADIST
            SMALLEST = TARGET_ADIST
TARGET_DIST


# real weight dan hanya dikali oleh distance

# In[266]:


#Total Value for SOURCE
SOURCE_TV = []

for i in range(0, len(SOURCE_DIST)):
    FEATURE_TIME = 1
    for j in range(0, len(REAL_WEIGHTMATRIX)):
        FEATURE_TIME *= REAL_WEIGHTMATRIX[j]
    SOURCE_TV.append(SOURCE_DIST[i] * FEATURE_TIME)
SOURCE_TV


# In[267]:


#Total Value for TARGET
TARGET_TV = []

for i in range(0, len(TARGET_DIST)):
    FEATURE_TIME = 1
    for j in range(0, len(REAL_WEIGHTMATRIX)):
        FEATURE_TIME *= REAL_WEIGHTMATRIX[j]
    TARGET_TV.append(TARGET_DIST[i] * FEATURE_TIME)
TARGET_TV


# ### **Penggunaan rumus 5**
# 
# pada tahapan ini baru dilakukan perhitungan jarak antara souce to target dimana ditentukan gap nilai dari source ke target. nilai gap tersebut akan disimpan pada gap_source_to_target. lalu didapat prediksi dari target y dengan melihat nili terkecil

# In[268]:


#predicts TARGET Label Belongings
TARGET_PRED = []

for i in range(0, len(TARGET_TV)):
    GAP_SOURCE_TO_TARGET = []
    for j in range(0, len(SOURCE_TV)):
        GAP_SOURCE_TO_TARGET.append(np.absolute(SOURCE_TV[j]- TARGET_TV[i]))
    TARGET_PRED.append(SOURCE_CHOSEY[np.argmin(GAP_SOURCE_TO_TARGET)])
    
TARGET_PRED


# In[269]:



clf = svm.LinearSVC(C=1.0, dual=True, fit_intercept=True,
          intercept_scaling=1, loss='squared_hinge', max_iter=1000,
          multi_class='ovr', penalty='l2', random_state=None, tol=0.0001,
          verbose=0)
# Fit model using features, X, and labels, Y

clf.fit(SOURCE_CHOSEX, SOURCE_CHOSEY, GAP_SOURCE_TO_TARGET)

PRED_RESULT = clf.predict(SOURCE_CHOSEX)

NEW_TARGET_TRAIN_X, NEW_TARGET_TEST_X, NEW_TARGET_TRAIN_Y, NEW_TARGET_TEST_Y = train_test_split(TARGET_CHOSEX, PRED_RESULT)

clf.fit(NEW_TARGET_TRAIN_X, NEW_TARGET_TRAIN_Y)

NEW_PRED_RESULT = clf.predict(NEW_TARGET_TEST_X)

matrix = confusion_matrix(NEW_TARGET_TEST_Y, NEW_PRED_RESULT)
print(matrix)
metric_akur = accuracy_score(NEW_TARGET_TEST_Y, NEW_PRED_RESULT)
print(metric_akur)
metric_preci = precision_score(NEW_TARGET_TEST_Y, NEW_PRED_RESULT, average='weighted')
print(metric_preci)
metric_recal = recall_score(NEW_TARGET_TEST_Y, NEW_PRED_RESULT,  average='weighted')
print(metric_recal)
metric_balap = f1_score(NEW_TARGET_TEST_Y, NEW_PRED_RESULT, average='weighted')
print(metric_balap)

#no policy
DATA_EXCEL.append([PREFIX_SOURCE+" "+PREFIX_TARGET+" LS-3"])
DATA_EXCEL.append(["Accuracy"]);
DATA_EXCEL.append([metric_akur]);
DATA_EXCEL.append(["Precision"]);
DATA_EXCEL.append([metric_preci]);
DATA_EXCEL.append(["Recal"]);
DATA_EXCEL.append([metric_recal]);
DATA_EXCEL.append(["F1"]);
DATA_EXCEL.append([metric_balap]);

means_df = pd.DataFrame(DATA_EXCEL)
means_df.to_excel("result_with_pseudolabel_data_target_feature_transformation_"+PREFIX_SOURCE+" "+PREFIX_TARGET+".xlsx", index=False, header=False)




# means_df = pd.DataFrame(confusion_matrix(MNIST_CHOSEY, SVHN_PRED))
# means_df.to_excel("measurements_metrics.xlsx")


# 
