import math
import numpy as np
import tensorflow as tf
import glob 
import numpy as np
import pandas as pd
import math
import sys
import sklearn.metrics
import sklearn.neighbors
from sklearn.metrics import confusion_matrix, accuracy_score, precision_score, recall_score, f1_score
from sklearn import svm
from sklearn import model_selection
from sklearn import metrics
from sklearn import svm
from sklearn import neighbors
from sklearn.model_selection import train_test_split
from sklearn.pipeline import Pipeline
from sklearn.metrics import pairwise_distances
import scipy.optimize as opt


#python -m pip install tensorflow numpy pandas sklearn scikit-image open_pyxl xlrd


prefix_source = sys.argv[1];
prefix_target = sys.argv[2];

fs = np.asarray(pd.read_excel(r'%s_surf_10_feas.xlsx'%prefix_source))
ft = np.asarray(pd.read_excel(r'%s_surf_10_feas.xlsx'%prefix_target))
ts = np.asarray(pd.read_excel(r'%s_surf_10_label.xls'%prefix_source))
tt = np.asarray(pd.read_excel(r'%s_surf_10_label.xls'%prefix_target))

dist_list = []

excel_file = []

def calculate_euclidian_distance(v1, v2):
    return np.linalg.norm(v1 - v2)

def calculate_mmd(a, b, lena, lenb):
    return abs(( ((1/len_a)*a) - ((1/len_b)*b) ))

def calculate_mean_each_vector(vs):
    return np.average(np.asarray(vs), axis=0)

def mmd_func(a, b, len_a, len_b):
    return abs(( ((1/len_a)*a) - ((1/len_b)*b) ))

def calculate_mmd(fs_mean, ft_mean):
    mmd = []
    for i in range(len(fs_mean)):
        mmd.append((mmd_func(fs_mean[i], ft_mean[i], len(fs_mean), len(ft_mean)))**2)
    return mmd

def get_treshold(mmd):
    return np.mean(np.asarray(mmd))

def process_feature_selection_and_calculate_weight(mmd):
    treshold = get_treshold(mmd)
    trimmed = []
    total_dist_list = np.sum(mmd)
    total_element_dist_list = len(mmd)
    feature_vector = []
    for i in mmd:
        if i >= treshold:
            feature_vector.append(0)
        else:
            feature_vector.append(
                ((1/total_element_dist_list)*total_dist_list)/i
            )
    return feature_vector

def feature_transformation_version_1(fs, feature_vector):

    for fs_i in fs:
        #apply formula for feature transformation 1
        fs_i = [a*b for a, b in zip(fs_i, feature_vector)]
    return fs

def get_minimal_distance(fs_v1_i, center_cluster, label):
    this_list_distance = []
    for i in center_cluster.keys():
        cc_i = center_cluster[i]
        # add all distance result
        this_list_distance.append(calculate_euclidian_distance(cc_i, fs_v1_i))
    return this_list_distance[np.argmin(this_list_distance)]


def feature_transformation_version_2(fs_v1, ts, center_cluster):
    #pick the right center of cluster
    j = 0
    for i in fs_v1:
        #pick the center of cluster which the center has same y label with the vector x
        center_cluster_choosed = center_cluster["%s"%ts[j][0]]
        #apply the formula for the second transformation
        i = (np.asarray([a*b for a,b in zip(i, center_cluster_choosed)])*np.sum(center_cluster_choosed))/(get_minimal_distance(i, center_cluster, ts))
        j=j+1
    return fs_v1

def calculate_center(fs, ts):
    source_centre = {}
    #initialize array
    for j in range(0, len(ts)):
        source_centre["%s"%ts[j][0]] = []

    #regroup each vector to each target
    for j in range(0, len(ts)):
        source_centre["%s"%ts[j][0]].append(fs[j]);
    #process center
    for i in source_centre.keys():
        source_centre[i] = np.sum(source_centre[i], axis=0)/len(source_centre[i])
    return source_centre

def apply_weights(vector, weight):
    result = []
    for i in vector:
        result.append([])
        for j in i:
            result[-1].append(np.sum(np.multiply(weight,j)))
    return result

    
def apply_knn(fs, ts, ft, tt, process, sample_weight):
    k = 6
    def knnWeights(weights , X,y , k):
        def dist(a,b):
            return np.sum(np.multiply(weights, np.abs(a-b)))
        distMat = pairwise_distances(X,metric=dist)
        kneighbors = np.argsort(distMat , axis=1)[:,1:k+1]
        return kneighbors
    
    weights_init = np.zeros(fs.shape[1])
    k = 2
    wpairs = knnWeights(weights_init, fs, ft, k)
    lweight = weights_init
    for i in wpairs:
        lweight = lweight + np.abs(fs[i[0]] - fs[i[1]])
    def w_dist(x, y, **kwargs):
        return sum(kwargs["weights"]*((x-y)*(x-y)))
    if sample_weight != None:
        fs = apply_weights(fs, sample_weight)
    knn = neighbors.KNeighborsClassifier(metric='minkowski', p=2, 
                           metric_params={'w': lweight})
    
    knn.fit(fs, ts)

    pred_result = knn.predict(ft)

    matrix = confusion_matrix(tt, pred_result)
    print(matrix)
    metric_akur = accuracy_score(tt, pred_result)
    print(metric_akur)
    metric_preci = precision_score(tt, pred_result, average='weighted')
    print(metric_preci)
    metric_recal = recall_score(tt, pred_result,  average='weighted')
    print(metric_recal)
    metric_balap = f1_score(tt, pred_result, average='weighted')
    print(metric_balap)

    excel_file.append(["%s %s LS-%s"%(prefix_source, prefix_target, process)])
    excel_file.append(["Accuracy"]);
    excel_file.append([metric_akur]);
    excel_file.append(["Precision"]);
    excel_file.append([metric_preci]);
    excel_file.append(["Recal"]);
    excel_file.append([metric_recal]);
    excel_file.append(["F1"]);
    excel_file.append([metric_balap]);

def process():
    #LS1
    fs_mean = calculate_mean_each_vector(fs)
    ft_mean = calculate_mean_each_vector(ft)
    
    mmd = calculate_mmd(fs_mean, ft_mean)
    #LS2
    feature_vector_sav = process_feature_selection_and_calculate_weight(mmd)
    print(np.asarray(feature_vector_sav).shape)
    #LS3
    featureS_transform_v1_sav = feature_transformation_version_1(fs, feature_vector_sav)

    featureT_transform_v1_sav = feature_transformation_version_1(ft, feature_vector_sav)
    center_cluster = calculate_center(featureS_transform_v1_sav, ts)
    
    featureT_transform_v2_sav = feature_transformation_version_2(featureT_transform_v1_sav, tt, center_cluster)

    apply_knn(fs, ts, ft, tt, 1, mmd)
    apply_knn(fs, ts, ft, tt, 2, feature_vector_sav)
    apply_knn(featureS_transform_v1_sav, ts, featureT_transform_v2_sav, tt, 3, None)

    means_df = pd.DataFrame(excel_file)
    means_df.to_excel("result_with_feaure_transformation_on_source_and_target_using_weighted_knn_task_3c1 "+prefix_source+" "+prefix_target+".xlsx", index=False, header=False)
process();