#Preparing Data

#Source (DSLR)
#Target (Webcam)
from skimage.io import imread, imshow
from skimage.transform import rescale, resize
import math
import glob
import numpy as np

RESCALE_WIDTH = 400
RESCALE_HEIGHT = 400

image_source = imread('DSLR_Dipilih/headphones/frame_0001.jpg');
print(image_source.shape)

image_target = imread('Webcam_Dipilih/headphones/frame_0001.jpg');

print(image_target.shape)

print('Rescale the image to larger pixel')

how_much_rescale_x = min(image_target.shape[0], image_source.shape[0])/max(image_target.shape[0],image_source.shape[0])

how_much_rescale_y = min(image_target.shape[1], image_source.shape[1])/max(image_target.shape[1], image_source.shape[1])

how_much_rescale = min(how_much_rescale_x, how_much_rescale_y)
apply_to = ''


if(image_target.shape[0] > image_source.shape[0]):
    apply_to = 'target'
else:
    apply_to = 'source'

image_list = [
    'back_pack',
    'bike',
    'calculator',
    'headphones',
    'keyboard',
    'laptop_computer',
    'monitor',
    'mouse',
    'mug',
    'projector'
]

SOURCE = "DSLR_Dipilih"
TARGET = "Webcam_Dipilih"

SOURCE_X = []
SOURCE_Y = []
TARGET_X = []
TARGET_Y = []

for _image_list in image_list:
    image_specific_list = glob.glob(SOURCE+"/"+_image_list+"/*")
    for _image_specific_list in image_specific_list:
        image_source = resize(image_source, (RESCALE_WIDTH, RESCALE_HEIGHT, 3), anti_aliasing=False)
        image_source_feature_matrix = np.zeros((image_source.shape[0], image_source.shape[1]))

        print(image_source)

        for i in range(0,image_source.shape[0]):
            for j in range(0,image_source.shape[1]):
                image_source_feature_matrix[i][j] = float((image_source[i,j,0] + image_source[i,j,1] + image_source[i,j,2])/3)
                print(image_source_feature_matrix[i][j])
        image_source_feat = np.reshape(image_source_feature_matrix, (image_source.shape[0]*image_source.shape[1]))
        SOURCE_X.append(image_source_feat)
        SOURCE_Y.append(_image_list)

for _image_list in image_list:
    image_specific_list = glob.glob(TARGET+"/"+_image_list+"/*")
    for _image_specific_list in image_specific_list:
        image_target = resize(image_target, (RESCALE_WIDTH, RESCALE_HEIGHT, 3), anti_aliasing=False)
        image_target_feature_matrix = np.zeros((image_target.shape[0], image_target.shape[1]))

        for i in range(0,image_target.shape[0]):
            for j in range(0,image_target.shape[1]):
                image_target_feature_matrix[i][j] = float((image_target[i,j,0] + image_target[i,j,1] + image_target[i,j,2])/3)
                print(image_target_feature_matrix[i][j])
        image_target_feat = np.reshape(image_target_feature_matrix, (image_target.shape[0]*image_target.shape[1]))
        TARGET_X.append(image_source_feat)
        TARGET_Y.append(_image_list)

print(len(SOURCE_X))
print(len(SOURCE_Y))
print(len(TARGET_X))
print(len(TARGET_Y))
#print(glob.glob("DSLR_Dipilih/headphones/*"))

