import numpy as np
a = [[2, 3], [4, 6]]
b = [1, 2, 3, 4]

def apply_weights(vector, weight):
    result = []
    for i in vector:
        result.append([])
        for j in i:
            result[-1].append(np.sum(np.multiply(weight,j)))
    return result
print(apply_weights(a, b))
print(a * b)