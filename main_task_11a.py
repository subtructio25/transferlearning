import math
import numpy as np
import tensorflow as tf
import glob 
import numpy as np
import pandas as pd
import math
import sys
import sklearn.metrics
import sklearn.neighbors
from sklearn.metrics import confusion_matrix, accuracy_score, precision_score, recall_score, f1_score
from sklearn import svm
from sklearn import model_selection
from sklearn import metrics
from sklearn import svm
from sklearn.model_selection import train_test_split

import pandas
import scipy.stats as stats
from statsmodels.stats.multicomp import pairwise_tukeyhsd
from statsmodels.stats.libqsturng import psturng

#python -m pip install tensorflow numpy pandas sklearn scikit-image open_pyxl xlrd


prefix_source = sys.argv[1];
prefix_target = sys.argv[2];
c_val = sys.argv[3];

if c_val == None:
    c_val = 1.0

fs = np.asarray(pd.read_excel(r'%s_surf_10_feas.xlsx'%prefix_source))
ft = np.asarray(pd.read_excel(r'%s_surf_10_feas.xlsx'%prefix_target))
ts = np.asarray(pd.read_excel(r'%s_surf_10_label.xls'%prefix_source))
tt = np.asarray(pd.read_excel(r'%s_surf_10_label.xls'%prefix_target))

dist_list = []

excel_file = []

def calculate_euclidian_distance(v1, v2):
    return np.linalg.norm(v1 - v2)

def calculate_mmd(a, b, lena, lenb):
    return abs(( ((1/len_a)*a) - ((1/len_b)*b) ))

def calculate_mean_each_vector(vs):
    return np.average(np.asarray(vs), axis=0)

def mmd_func(a, b, len_a, len_b):
    return abs(( ((1/len_a)*a) - ((1/len_b)*b) ))

def calculate_mmd(fs_mean, ft_mean):
    mmd = []
    for i in range(len(fs_mean)):
        mmd.append((mmd_func(fs_mean[i], ft_mean[i], len(fs_mean), len(ft_mean)))**2)
    return mmd

def get_treshold(mmd):
    return np.mean(np.asarray(mmd))

def feature_selection(mmd):
    treshold = get_treshold(mmd)
    trimmed = []
    total_dist_list = np.sum(mmd)
    total_element_dist_list = len(mmd)
    feature_vector = []
    k = 0
    for i in mmd:
        if i < treshold:
            feature_vector.append(k)
        k+=1
    return feature_vector

def process_feature_selection_and_calculate_weight(mmd):
    treshold = get_treshold(mmd)
    trimmed = []
    total_dist_list = np.sum(mmd)
    total_element_dist_list = len(mmd)
    feature_vector = []
    for i in mmd:
        if i >= treshold:
            feature_vector.append(0)
        else:
            feature_vector.append(
                ((1/total_element_dist_list)*total_dist_list)/i
            )
    return feature_vector

def feature_transformation_version_1(fs, feature_vector):

    for fs_i in fs:
        #apply formula for feature transformation 1
        fs_i = [a*b for a, b in zip(fs_i, feature_vector)]
    return fs

def get_minimal_distance(fs_v1_i, center_cluster, label):
    this_list_distance = []
    for i in center_cluster.keys():
        cc_i = center_cluster[i]
        # add all distance result
        this_list_distance.append(calculate_euclidian_distance(cc_i, fs_v1_i))
    return this_list_distance[np.argmin(this_list_distance)]


def feature_transformation_version_2(fs_v1, ts, center_cluster):
    #pick the right center of cluster
    j = 0
    for i in fs_v1:
        #pick the center of cluster which the center has same y label with the vector x
        center_cluster_choosed = center_cluster["%s"%ts[j][0]]
        #apply the formula for the second transformation
        i = (np.asarray([a*b for a,b in zip(i, center_cluster_choosed)])*np.sum(center_cluster_choosed))/(get_minimal_distance(i, center_cluster, ts))
        j=j+1
    return fs_v1

def calculate_center(fs, ts):
    source_centre = {}
    #initialize array
    for j in range(0, len(ts)):
        source_centre["%s"%ts[j][0]] = []

    #regroup each vector to each target
    for j in range(0, len(ts)):
        source_centre["%s"%ts[j][0]].append(fs[j]);
    #process center
    for i in source_centre.keys():
        source_centre[i] = np.sum(source_centre[i], axis=0)/len(source_centre[i])
    return source_centre

def apply_svm(fs, ts, ft, tt, process, sample_weight, version = 1):
    if version == 1:
        clf = svm.LinearSVC(C=float(c_val), dual=True, fit_intercept=True,
              intercept_scaling=1, loss='squared_hinge', max_iter=4000,
              multi_class='ovr', penalty='l2', random_state=None, tol=0.0001,
              verbose=0)
    elif version == 2:
        clf = svm.SVC(C=float(c_val), kernel="rbf", gamma="auto", class_weight="balanced", max_iter=4000)
        
    # Fit model using features, X, and labels, Y
    if(len(sample_weight) > 0):
        clf.fit(fs, ts, sample_weight);
    else :
        clf.fit(fs, ts)

    pred_result = clf.predict(ft)

    matrix = confusion_matrix(tt, pred_result)
    print(matrix)
    metric_akur = accuracy_score(tt, pred_result)
    print(metric_akur)
    metric_preci = precision_score(tt, pred_result, average='weighted')
    print(metric_preci)
    metric_recal = recall_score(tt, pred_result,  average='weighted')
    print(metric_recal)
    metric_balap = f1_score(tt, pred_result, average='weighted')
    print(metric_balap)

    excel_file.append(["%s %s LS-%s"%(prefix_source, prefix_target, process)])
    excel_file.append(["Accuracy"]);
    excel_file.append([metric_akur]);
    excel_file.append(["Precision"]);
    excel_file.append([metric_preci]);
    excel_file.append(["Recal"]);
    excel_file.append([metric_recal]);
    excel_file.append(["F1"]);
    excel_file.append([metric_balap]);

def feature_transformation_version_3(fs):
    fs1 = pandas.DataFrame(fs);
    fs1 = pandas.melt(fs1.reset_index(), id_vars=['index'], value_vars=range(1,len(fs[0])))
    fs1.columns = ['index', 'treatments', 'value']
    res = pairwise_tukeyhsd(endog=fs1['value'], groups=fs1['treatments'], alpha=0.01)
    tukey_data = pd.DataFrame(data=res._results_table.data[1:], columns = res._results_table.data[0])
    # meandiff
    tukey_data = tukey_data.query("reject == False")
    tukey_data = tukey_data.sort_values('meandiff',ascending=False).head(100)
    result  = np.concatenate(([i[0] for i in np.vstack(tukey_data['group1'])], [i[0] for i in np.vstack(tukey_data['group2'])]))
    return np.asarray(list(set(result)))

def process():
    #LS1
    fs_mean = calculate_mean_each_vector(fs)
    ft_mean = calculate_mean_each_vector(ft)

    print(prefix_source+" "+prefix_target)
    
    mmd = calculate_mmd(fs_mean, ft_mean)
    #LS2
    chosen = feature_selection(mmd)
    chosen_fs = fs[:, chosen]
    chosen_ts = ts
    chosen_ft = ft[:, chosen]
    chosen_tt = tt
    feature_vector_sav = process_feature_selection_and_calculate_weight(mmd)
    #LS3
    featureS_transform_v1_sav = feature_transformation_version_1(fs, feature_vector_sav)
    rS_transform_v3 = feature_transformation_version_3(featureS_transform_v1_sav)
    featureS_transform_v3_sav = featureS_transform_v1_sav[:, rS_transform_v3]

    featureT_transform_v1_sav = feature_transformation_version_1(ft, feature_vector_sav)
    rT_transform_v3 = feature_transformation_version_3(featureT_transform_v1_sav)
    center_cluster = calculate_center(featureT_transform_v1_sav, tt)
    featureT_transform_v2_sav = feature_transformation_version_2(featureT_transform_v1_sav, tt, center_cluster)

    chosen_fS_transform_v3 = featureS_transform_v3_sav
    chosen_fT_transform_v2 = featureT_transform_v2_sav[:, rT_transform_v3]

    chosen_len = np.max([chosen_fS_transform_v3.shape[1], chosen_fT_transform_v2.shape[1]])
    print("chosen len")
    print(chosen_len)
    if chosen_len != chosen_fS_transform_v3.shape[1]:
        chosen_fS_transform_v31 = chosen_fS_transform_v3.copy()
        chosen_fS_transform_v31.resize((chosen_fS_transform_v31.shape[0], chosen_len), refcheck=False)
        chosen_fS_transform_v3 = chosen_fS_transform_v31.copy()
    elif chosen_len != chosen_fT_transform_v2.shape[1]:
        chosen_fT_transform_v2b = chosen_fT_transform_v2.copy()
        chosen_fT_transform_v2b.resize((chosen_fT_transform_v2b.shape[0], chosen_len), refcheck=False)
        chosen_fT_transform_v2 = chosen_fT_transform_v2b.copy()

   
    apply_svm(fs, ts, ft, tt, 1, np.random.dirichlet(np.ones(np.dot(fs, mmd).shape[0]),size=1)[0], 1)
    apply_svm(chosen_fs, chosen_ts, chosen_ft, chosen_tt, 2, {}, 1)
    apply_svm(chosen_fS_transform_v3, chosen_ts, chosen_fT_transform_v2, chosen_tt, 3, {}, 1)

    means_df = pd.DataFrame(excel_file)
    means_df.to_excel("result_with_feaure_transformation_on_source_and_target_task11a_with_C_val_"+c_val+" "+prefix_source+" "+prefix_target+".xlsx", index=False, header=False)
process();