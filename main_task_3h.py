import math
import numpy as np
import tensorflow as tf
import glob 
import numpy as np
import pandas as pd
import math
import sys
import sklearn.metrics
import sklearn.neighbors
from sklearn.metrics import confusion_matrix, accuracy_score, precision_score, recall_score, f1_score
from sklearn import svm
from sklearn import model_selection
from sklearn import metrics
from sklearn import svm
from sklearn.model_selection import train_test_split

#python -m pip install tensorflow numpy pandas sklearn scikit-image open_pyxl xlrd


prefix_source = sys.argv[1];
prefix_target = sys.argv[2];

fs = np.asarray(pd.read_excel(r'%s_surf_10_feas.xlsx'%prefix_source))
ft = np.asarray(pd.read_excel(r'%s_surf_10_feas.xlsx'%prefix_target))
ts = np.asarray(pd.read_excel(r'%s_surf_10_label.xls'%prefix_source))
tt = np.asarray(pd.read_excel(r'%s_surf_10_label.xls'%prefix_target))

dist_list = []

excel_file = []

def calculate_euclidian_distance(v1, v2):
    return np.linalg.norm(v1 - v2)

def calculate_mmd(a, b, lena, lenb):
    return abs(( ((1/len_a)*a) - ((1/len_b)*b) ))

def calculate_mean_each_vector(vs):
    return np.average(np.asarray(vs), axis=0)

def mmd_func(a, b, len_a, len_b):
    return abs(( ((1/len_a)*a) - ((1/len_b)*b) ))

def calculate_mmd(fs_mean, ft_mean):
    mmd = []
    for i in range(len(fs_mean)):
        mmd.append((mmd_func(fs_mean[i], ft_mean[i], len(fs_mean), len(ft_mean)))**2)
    return mmd

def get_treshold(mmd):
    return np.mean(np.asarray(mmd))

def process_feature_selection_and_calculate_weight(mmd):
    treshold = get_treshold(mmd)
    trimmed = []
    total_dist_list = np.sum(mmd)
    total_element_dist_list = len(mmd)
    feature_vector = []
    for i in mmd:
        if i >= treshold:
            feature_vector.append(0)
        else:
            feature_vector.append(
                ((1/total_element_dist_list)*total_dist_list)/i
            )
    return feature_vector

def feature_selection(mmd):
    treshold = get_treshold(mmd)
    trimmed = []
    total_dist_list = np.sum(mmd)
    total_element_dist_list = len(mmd)
    feature_vector = []
    k = 0
    for i in mmd:
        if i < treshold:
            feature_vector.append(k)
        k+=1
    return feature_vector

def feature_transformation_version_1(fs, chosen):
    return fs[:, chosen]

def get_minimal_distance(fs_v1_i, center_cluster, label):
    this_list_distance = []
    for i in center_cluster.keys():
        cc_i = center_cluster[i]
        # add all distance result
        this_list_distance.append(calculate_euclidian_distance(cc_i, fs_v1_i))
    return this_list_distance[np.argmin(this_list_distance)]


def feature_transformation_version_2(fs_v1, ts, center_cluster, weight):
    #pick the right center of cluster
    j = 0
    for i in fs_v1:
        #pick the center of cluster which the center has same y label with the vector x
        center_cluster_choosed = center_cluster["%s"%ts[j][0]]
        #apply the formula for the second transformation
        i = (i * center_cluster_choosed) / weight[j];
        j=j+1
    return fs_v1

def calculate_center(fs, ts):
    source_centre = {}
    #initialize array
    for j in range(0, len(ts)):
        source_centre["%s"%ts[j][0]] = []

    #regroup each vector to each target
    for j in range(0, len(ts)):
        source_centre["%s"%ts[j][0]].append(fs[j]);
    #process center
    for i in source_centre.keys():
        source_centre[i] = np.sum(source_centre[i], axis=0)/len(source_centre[i])
    return source_centre

def weighting(ft, center_cluster, tt):
    weight = []
    j = 0
    for i in ft:
        center_cluster_total = 0
        chose_center = center_cluster["%s"%tt[j][0]]
        for k in range(len(tt)):
            k = center_cluster["%s"%tt[k][0]]
            center_cluster_total += calculate_euclidian_distance(chose_center, k)
        distance = calculate_euclidian_distance(chose_center, i)
        weight.append(np.linalg.norm(distance/center_cluster_total))
        j+=1
    return weight

def apply_tl(fs, ts, ft, tt, process, sample_weight, version = 1):
    pred_result = nearest(fs, ft, ts)

    matrix = confusion_matrix(tt, pred_result)
    print(matrix)
    metric_akur = accuracy_score(tt, pred_result)
    print(metric_akur)
    metric_preci = precision_score(tt, pred_result, average='weighted')
    print(metric_preci)
    metric_recal = recall_score(tt, pred_result,  average='weighted')
    print(metric_recal)
    metric_balap = f1_score(tt, pred_result, average='weighted')
    print(metric_balap)

    excel_file.append(["%s %s LS-%s"%(prefix_source, prefix_target, process)])
    excel_file.append(["Accuracy"]);
    excel_file.append([metric_akur]);
    excel_file.append(["Precision"]);
    excel_file.append([metric_preci]);
    excel_file.append(["Recal"]);
    excel_file.append([metric_recal]);
    excel_file.append(["F1"]);
    excel_file.append([metric_balap]);

def nearest(fs, ft, ts):
    predict = []
    for i in range(len(ft)):
        distances = []
        for j in range(len(fs)):
            distances.append(calculate_euclidian_distance(fs[j], ft[i]))
        predict.append(ts[np.argmin(np.asarray(distances))])
    return predict

def process():
    #LS1
    fs_mean = calculate_mean_each_vector(fs)
    ft_mean = calculate_mean_each_vector(ft)
    
    mmd = calculate_mmd(fs_mean, ft_mean)
    #LS2
    chosen = feature_selection(mmd)
    featureS_transform_v1_sav = feature_transformation_version_1(fs, chosen)
    #LS3
    featureT_transform_v1_sav = feature_transformation_version_1(ft, chosen)

    ts_chosen = ts
    tt_chosen = tt

    center_cluster = calculate_center(featureS_transform_v1_sav, ts_chosen)
    weight = weighting(featureT_transform_v1_sav, center_cluster, tt_chosen)
    featureT_transform_v2_sav = feature_transformation_version_2(featureT_transform_v1_sav, tt_chosen, center_cluster, weight)

    apply_tl(featureS_transform_v1_sav, ts_chosen, featureT_transform_v1_sav, tt_chosen, 2, {})
    apply_tl(featureS_transform_v1_sav, ts_chosen, featureT_transform_v2_sav, tt_chosen, 3, {})

    means_df = pd.DataFrame(excel_file)
    means_df.to_excel("result_with_feaure_transformation_on_source_and_target_task3h"+prefix_source+" "+prefix_target+".xlsx", index=False, header=False)
process();